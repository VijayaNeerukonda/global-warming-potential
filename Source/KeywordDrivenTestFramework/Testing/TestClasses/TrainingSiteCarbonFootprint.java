/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteEnvSusPageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "Click on Monitoring Maintenance",
    createNewBrowserInstance = false
)
public class TrainingSiteCarbonFootprint extends BaseClass
{

    String error = "";

    public TrainingSiteCarbonFootprint()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!navigateToAPageFromEnvironmentalSustainabilityPage())
        {
            return narrator.testFailed("Failed to navigate to a module from Monitoring Maintenance page" + error);
        }
        return narrator.finalizeTest("Successfully Navigated to module from Monitoring Maintenance page");
    }

   
    public boolean navigateToAPageFromEnvironmentalSustainabilityPage() throws InterruptedException
    {
       
        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteEnvSusPageObjects.linkForAPageInHomePageXpath(testData.getData("Carbon FootprintPageName")))) {
            error = "Failed to locate the module: "+testData.getData("Carbon FootprintPageName");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteEnvSusPageObjects.linkForAPageInHomePageXpath(testData.getData("Carbon FootprintPageName"))))
        {
            error = "Failed to navigate to the module: "+testData.getData("Carbon FootprintPageName");
            return false;
        }

        
        return true;

    }

}
