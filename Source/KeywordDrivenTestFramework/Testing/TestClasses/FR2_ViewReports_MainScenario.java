/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.FR2_Viewreports_MainScenario_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.GlobalWarmingPotential_PageObjects;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "FR2_ViewReports_MainScenario",
    createNewBrowserInstance = false
)
public class FR2_ViewReports_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;
   

  public FR2_ViewReports_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!viewGWPRecords())
        {
            return narrator.testFailed("Failed to view Global Warming Potential Reports: " + error);
        }
        return narrator.finalizeTest("Successfully viewed Global Warming Potential Reports");
    }

   
    public boolean viewGWPRecords() throws InterruptedException
    {    
          //Set the page as a parent page
        String parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        
       //Global Warming Potential module
        if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.globalWarmingPotentialModule()))
        {
            error = "Failed to locate Global Warming Potential module";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(GlobalWarmingPotential_PageObjects.globalWarmingPotentialModule()))
        {
            error = "Failed to click on Global Warming Potential module";
            return false;
        }
        
        
        pause(8000);
         //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_Viewreports_MainScenario_PageObjects.searchButton())) 
        {
            error = "Failed to locate Search button";
            return false;
        }
        
       if (!SeleniumDriverInstance.clickElementbyXpath(FR2_Viewreports_MainScenario_PageObjects.searchButton())) 
        {
            error = "Failed to click Search button";
            return false;
        }
        
       pause(5000);
         //Reports Button on search page
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_Viewreports_MainScenario_PageObjects.reportsButton())) 
        {
            error = "Failed to locate Reports Button on search page";
            return false;
        }
        
       if (!SeleniumDriverInstance.clickElementbyXpath(FR2_Viewreports_MainScenario_PageObjects.reportsButton())) 
        {
            error = "Failed to click Reports Button on search page";
            return false;
        }
        
       
        //Click view report
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_Viewreports_MainScenario_PageObjects.viewReport())) 
        {
            error = "Failed to locate view report on right side";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_Viewreports_MainScenario_PageObjects.viewReport())) 
        {
            error = "Failed to click view report on right side";
            return false;
        }
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
         //Click continue
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_Viewreports_MainScenario_PageObjects.continueButton())) 
        {
            error = "Failed to locate continue button on popup";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_Viewreports_MainScenario_PageObjects.continueButton())) 
        {
            error = "Failed to click continue button on popup";
            return false;
        }
        
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
         pause(5000);
        //Scroll down
         JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
         js.executeScript("window.scrollBy(0,1000)");
         
          pause(2000);
          narrator.stepPassedWithScreenShot("Successfully showing all records in the View Report");
          
        //Scroll up
         js.executeScript("window.scrollBy(0,-1000)");
         
         pause(5000);
         //Export dropdown menu
          if (!SeleniumDriverInstance.waitForElementByXpath(FR2_Viewreports_MainScenario_PageObjects.exportDropdownMenu())) 
        {
            error = "Failed to locate Export dropdown menu";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_Viewreports_MainScenario_PageObjects.exportDropdownMenu())) 
        {
            error = "Failed to click Export dropdown menu";
            return false;
        }
        
         //Export Word
          if (!SeleniumDriverInstance.waitForElementByXpath(FR2_Viewreports_MainScenario_PageObjects.exportWord())) 
        {
            error = "Failed to locate Export Word";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_Viewreports_MainScenario_PageObjects.exportWord())) 
        {
            error = "Failed to click Export Word";
            return false;
        }
  
        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        System.out.println(SeleniumDriverInstance.Driver.getTitle());

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.swithToFrameByName("ifrMain");
        
        pause(3000);
        //Click Full report
        
//         if(!SeleniumDriverInstance.switchToWindow(SeleniumDriverInstance.Driver, FR3_Viewreports_MainScenario_PageObjects.fullReport())){
//            error = "Failed to switch to 'IsoMetrix' window.";
//            return false;
//        }
         
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_Viewreports_MainScenario_PageObjects.fullReport())) 
        {
            error = "Failed to locate Full report on right side";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_Viewreports_MainScenario_PageObjects.fullReport())) 
        {
            error = "Failed to click Full report on right side";
            return false;
        }
        
         //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
         //Click continue
        if (!SeleniumDriverInstance.waitForElementByXpath(FR2_Viewreports_MainScenario_PageObjects.continueButton())) 
        {
            error = "Failed to locate continue button on popup";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_Viewreports_MainScenario_PageObjects.continueButton())) 
        {
            error = "Failed to click continue button on popup";
            return false;
        }
        
        
        //switch to new window
        if(!SeleniumDriverInstance.switchToWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }
        
         pause(5000);
        //Scroll down
        js.executeScript("window.scrollBy(0,1000)");
         
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully showing all records in the View Report");
          
        //Scroll up
         js.executeScript("window.scrollBy(0,-1000)");
         
         pause(5000);
         //Export dropdown menu
          if (!SeleniumDriverInstance.waitForElementByXpath(FR2_Viewreports_MainScenario_PageObjects.exportDropdownMenu())) 
        {
            error = "Failed to locate Export dropdown menu";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_Viewreports_MainScenario_PageObjects.exportDropdownMenu())) 
        {
            error = "Failed to click Export dropdown menu";
            return false;
        }
        
         //Export Word
          if (!SeleniumDriverInstance.waitForElementByXpath(FR2_Viewreports_MainScenario_PageObjects.exportWord())) 
        {
            error = "Failed to locate Export Word";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(FR2_Viewreports_MainScenario_PageObjects.exportWord())) 
        {
            error = "Failed to click Export Word";
            return false;
        }
  
        //Close the current window
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        System.out.println(SeleniumDriverInstance.Driver.getTitle());

        //switch to parent window iframe
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.swithToFrameByName("ifrMain");
      
        return true;
        
        
    }

}
