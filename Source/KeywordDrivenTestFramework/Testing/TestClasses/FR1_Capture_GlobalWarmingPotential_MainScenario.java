/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.GlobalWarmingPotential_PageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "FR1_Capture_GlobalWarmingPotential_MainScenario",
    createNewBrowserInstance = false
)

public class FR1_Capture_GlobalWarmingPotential_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;
   

  public FR1_Capture_GlobalWarmingPotential_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!addGlobalWarmingPotentialRecord())
        {
            return narrator.testFailed("Failed to add Global Warming Potential record: " + error);
        }
        return narrator.finalizeTest("Successfully added Global Warming Potential record");
    }

   
    public boolean addGlobalWarmingPotentialRecord() throws InterruptedException
    {    
        //Global Warming Potential module
        if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.globalWarmingPotentialModule()))
        {
            error = "Failed to locate Global Warming Potential module";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(GlobalWarmingPotential_PageObjects.globalWarmingPotentialModule()))
        {
            error = "Failed to click on Global Warming Potential module";
            return false;
        }
        
        //Add button
         pause(8000);
        if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.addButtonXpath()))
        {
            error = "Failed to locate Global Warming Potential add button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(GlobalWarmingPotential_PageObjects.addButtonXpath()))
        {
            error = "Failed to click on Global Warming Potential add button";
            return false;
        }
        
        //processflow
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(GlobalWarmingPotential_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(GlobalWarmingPotential_PageObjects.processFlowStatus("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }

       
        //Year of compliance Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.yearOfComplianceDropdown()))
        {
            error = "Failed to locate Year of compliance Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(GlobalWarmingPotential_PageObjects.yearOfComplianceDropdown()))
        {
            error = "Failed to click on Year of compliance Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.singleSelect(testData.getData("Year of compliance"))))
        {
            error = "Failed to locate Year of compliance Dropdown value";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(GlobalWarmingPotential_PageObjects.singleSelect(testData.getData("Year of compliance"))))
        {
            error = "Failed to click on Year of compliance Dropdown value";
            return false;
        }
        
        //CO2
        if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.CO2Textbox()))
        {
            error = "Failed to locate CO2 field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(GlobalWarmingPotential_PageObjects.CO2Textbox(),testData.getData("CO2")))
        {
            error = "Failed to enter text in CO2 field";
            return false;
        }
        
        //CH4
        if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.CH4Textbox()))
        {
            error = "Failed to locate CH4 field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(GlobalWarmingPotential_PageObjects.CH4Textbox(),testData.getData("CH4")))
        {
            error = "Failed to enter text in CH4 field";
            return false;
        }
        
        //N2O
        if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.N2OTextbox()))
        {
            error = "Failed to locate N2O field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(GlobalWarmingPotential_PageObjects.N2OTextbox(),testData.getData("N2O")))
        {
            error = "Failed to enter text in N2O field";
            return false;
        }
        
        //Source
        if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.sourceTextbox()))
        {
            error = "Failed to locate Source field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(GlobalWarmingPotential_PageObjects.sourceTextbox(),testData.getData("Source")))
        {
            error = "Failed to enter text in Source field";
            return false;
        }
        
        //Save button
         if (!SeleniumDriverInstance.waitForElementByXpath(GlobalWarmingPotential_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(GlobalWarmingPotential_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        } 
        
        if (!SeleniumDriverInstance.WaitForLoaderToDisappear(GlobalWarmingPotential_PageObjects.saveWait())) {
            error = "Failed to wait for Saving…";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementsByXpath(GlobalWarmingPotential_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(GlobalWarmingPotential_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        narrator.stepPassedWithScreenShot("Successfully saved record and Processflow moves to Edit phase");
       String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(GlobalWarmingPotential_PageObjects.processFlowStatus("Edit phase"),"class");
        if (!processStatusEdit.equalsIgnoreCase("step active")) {
        error = "Failed to move to Edit phase";
        return false;
        }
        
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(GlobalWarmingPotential_PageObjects.GWPRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
       
        
        return true;
        
    }

}
