/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

//import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.*;
//import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.*;
import KeywordDrivenTestFramework.Testing.PageObjects.FR2_Viewreports_MainScenario_PageObjects.*;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class FR2_Viewreports_MainScenario_PageObjects extends BaseClass
{
     
    public static String searchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }  
    
    public static String reportsButton()
    {
        return "//div[@id='btnReports']";
    }   

    //   //span[@original-title='Reports']
    
    public static String viewReport()
    {
        return "//span[@title='View report ']";
    }   
     
//    public static String continueButton()
//    {
//        return "//div[@title='Continue']";
//    }
    
    public static String continueButton()
    {
        return "//div[@id='btnConfirmYes']";
    }
    
    public static String exportDropdownMenu()
    {
        return "//img[@id='viewer_ctl05_ctl04_ctl00_ButtonImgDown']";
    }
     
    public static String exportWord()
    {
        return "//a[@title='Word']";
    }   
    
    
     public static String fullReport()
    {
        return "//span[@title='Full report ']";
    }    
     
}
