/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

//import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.*;
//import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.*;
import KeywordDrivenTestFramework.Testing.PageObjects.GlobalWarmingPotential_PageObjects.*;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class GlobalWarmingPotential_PageObjects extends BaseClass
{
    //FR1 MS
    public static String globalWarmingPotentialModule()
    {
        return ".//div[@original-title='Global Warming Potential']";
    }
    
    public static String addButtonXpath()
    {
        return "//div[@id='btnActAddNew']";
    }
     
    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_AB78FE90-440E-4899-805C-C5BE7ED970DB']";
    }

    public static String processFlowStatus(String phase)
    {
    return "(//div[text()='"+phase+"'])[2]/parent::div";
    }
    
    public static String processFlowAddText()
    {
        return "(//div[text()='Add phase'])[2]";
    }
    
     public static String processFlowEditText()
    {
        return "(//div[text()='Edit phase'])[2]";
    }
       
    public static String yearOfComplianceDropdown()
    {
        return "//div[@id='control_7DB15062-2AF3-4E49-9A62-C91357D2A602']//li";
    }  
    
    public static String singleSelect(String value)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='"+value+"']";
    } 
    
    public static String CO2Textbox()
    {
        return "(//div[@id='control_01913909-2E6D-4FC3-B816-A3DD92B9F115']//input)[1]";
    }
    
    public static String CH4Textbox()
    {
        return "(//div[@id='control_CAA21AFA-839F-4876-8537-E1D5CE6BFF9D']//input)[1]";
    }
    
    public static String N2OTextbox()
    {
        return "(//div[@id='control_8F584CEC-25A4-44A6-A7EB-73139F3A55F9']//input)[1]";
    }
    
    public static String sourceTextbox()
    {
        return "(//div[@id='control_2A984FB8-32F4-4432-AAB8-9C0C7A5336DC']//input)[1]";
    }
    
    public static String saveButton()
    {
        return " //div[@id='btnSave_form_AB78FE90-440E-4899-805C-C5BE7ED970DB']";
    }
    
    public static String saveWait(){
        return ".//div[text()='Saving...']";
    }

    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String GWPRecordNumber_xpath() {
        return "(//div[@id='form_AB78FE90-440E-4899-805C-C5BE7ED970DB']//div[contains(text(),'- Record #')])[1]";
    }
          
   
    
}
